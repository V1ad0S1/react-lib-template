module.exports = {
  root: true,
  env: { browser: true, es2021: true },
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:react-hooks/recommended",
    "plugin:storybook/recommended"
  ],
  ignorePatterns: ['dist', 'storybook-static'],
  parser: '@typescript-eslint/parser',
  plugins: ['simple-import-sort', 'import-quotes'],
  rules: {
    'no-console': ["warn", { "allow": ["warn", "error", "assert"] }],
    'no-shadow': 'error',
    'no-useless-concat': 'error',
    'prefer-template': 'error',
    '@typescript-eslint/no-unused-vars': ["error", { "argsIgnorePattern": "^_" }],
    '@typescript-eslint/no-explicit-any': ["off"],

    'no-restricted-exports': ["error", {
      "restrictDefaultExports": {
        "direct": true,
        "named": true,
        "defaultFrom": true,
        "namedFrom": true,
        "namespaceFrom": true,
      }
    }],

    'simple-import-sort/imports': 'error',
    'simple-import-sort/exports': 'error',

    'import-quotes/import-quotes': ['error', "double"],
  },
  overrides: [
    {
      "files": ["*.stories.*", "src/theme/theme.ts", "vite.config.ts"],
      "rules": {
        "no-restricted-exports": ['off'],
      }
    },
    // override "simple-import-sort" config
    {
      "files": ["*.js", "*.jsx", "*.ts", "*.tsx"],
      "rules": {
        "simple-import-sort/imports": [
          "error",
          {
            "groups": [
              // Packages `react` related packages come first.
              ["^react", "^@?\\w"],
              // Internal packages.
              ["^(@|here|another|internal|patterns)(/.*|$)"],
              // Side effect imports.
              ["^\\u0000"],
              // Parent imports. Put `..` last.
              ["^\\.\\.(?!/?$)", "^\\.\\./?$"],
              // Other relative imports. Put same-folder imports and `.` last.
              ["^\\./(?=.*/)(?!/?$)", "^\\.(?!/?$)", "^\\./?$"],
              // Style imports.
              ["^.+\\.?(css|less)$"],
            ],
          },
        ],
      },
    },
  ],
};
