# React Components Library Template

This repository provides a template for creating a React components library.


## Technologies Used

- [Storybook](https://storybook.js.org/): A tool for developing UI components in isolation.
- [Vitest](https://vitest.dev/): A simple and extensible test runner for JavaScript.
- [Less](https://lesscss.org/): CSS extension.


## Getting Started

To get started, follow these steps:

1. Clone this repository to your local machine.
1. Install dependencies by running `npm install`.
1. Start Storybook by running `npm run dev`.

Story example located in `src/elements/button/` directory.


## Testing

Check out the [Vitest documentation](https://vitest.dev/guide/).
An example of unit tests can be found in the `src/utils/add.test.ts`.


## Publishing

1. Patch package version in `package.json`
1. Run `npm run build`. Minimal `package.json` will be copied into `dist`
   folder automatically
1. cd into `dist/` folder and run publish:  
   `cd dist && npm publish`
