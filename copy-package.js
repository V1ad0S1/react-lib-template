import fs from "fs";

const packageJsonContent = fs.readFileSync('package.json', 'utf-8');
const packageJson = JSON.parse(packageJsonContent);
delete packageJson['scripts'];
delete packageJson['lint-staged'];
packageJson['main'] = "index.js";
packageJson['files'] = ['*'];
const purePackageJsonContent = JSON.stringify(packageJson, null, 2);

fs.writeFileSync('dist/package.json', purePackageJsonContent, 'utf-8');

console.log('package.json copied successfully');
