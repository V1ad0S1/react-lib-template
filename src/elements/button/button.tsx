import { FC } from "react";

import { add } from "@/utils/add";

import Plus from "./plus.svg?react";

import cn from "./button.module.less";

type ButtonProps = {
  variant?: 'default' | 'primary';
  a: number;
  b: number;
};

const Button: FC<ButtonProps> = ({ variant, a, b }) => {
  const classNames = [
    cn.button,
    variant === 'primary' ? cn.buttonPrimary : cn.buttonDefault,
  ];
  return (
    <button className={classNames.join(' ')} onClick={() => alert(add(a, b))}>
      {a}
      <Plus />
      {b}
    </button>
  );
};

export { Button };
