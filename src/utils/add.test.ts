import { expect, test } from "vitest";

import { add } from "./add";

test('Commutativity', () => {
  const first = 1;
  const second = 2;
  expect(add(first, second)).eq(add(second, first));
});

test('Add two integers', () => {
  expect(add(1, 2)).eq(3);
});

test('Add one to array elements', () => {
  const input = [1, 2, 3];
  const output = [2, 3, 4];
  expect(input.map(v => add(v, 1))).toEqual(output);
});
