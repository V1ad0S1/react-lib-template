import react from "@vitejs/plugin-react-swc";
import { glob } from "glob";
import { resolve } from "path";
import { defineConfig } from "vite";
import dts from "vite-plugin-dts";
import svgr from "vite-plugin-svgr";

import vars from "./src/theme/theme";
import pkg from "./package.json" assert { type: "json" };

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias: {
      '@/': new URL('./src/', import.meta.url).pathname,
    },
  },
  css: {
    preprocessorOptions: {
      less: {
        rootpath: 'src',
        modifyVars: vars,
      },
    },
  },
  plugins: [
    react(),
    svgr(),
    dts({ include: ['src'] }),
  ],
  build: {
    lib: {
      entry: glob.sync(resolve(__dirname, 'src/**/*.{ts,tsx}'), {
        ignore: [
          'src/vite-env.d.ts',
          'src/**/*.test.*',
          'src/**/*.stories.*',
        ],
      }),
      name: '',
      formats: ['es'],
    },
    copyPublicDir: false,
    rollupOptions: {
      output: {
        preserveModules: true,
        format: 'es',
      },
      external: [
        ...Object.keys(pkg.peerDependencies),
        ...Object.keys(pkg.devDependencies),
        'react/jsx-runtime',
        /^node:.*/,
      ],
    },
    minify: false,
  },
});
